-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 06 Novembre 2017 à 18:01
-- Version du serveur :  10.1.26-MariaDB-0+deb9u1
-- Version de PHP :  7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pokemon`
--

-- --------------------------------------------------------

--
-- Structure de la table `capacite`
--

CREATE TABLE `capacite` (
  `id_capacite` int(3) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `puissance` int(3) NOT NULL,
  `precision_cap` int(3) NOT NULL,
  `pp` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `capacite`
--

INSERT INTO `capacite` (`id_capacite`, `nom`, `type`, `categorie`, `puissance`, `precision_cap`, `pp`) VALUES
(1, 'Charge', 'Normal', 'physique', 40, 100, 35),
(2, 'Rugissement', 'Normal', 'status', 0, 100, 40),
(3, 'Griffe', '10', '1', 40, 100, 35),
(4, 'Mimi-Queue', '10', '2', 0, 100, 30),
(5, 'Pistolet a O', '4', '3', 40, 100, 25);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(3) NOT NULL,
  `nom_categorie` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom_categorie`) VALUES
(1, 'physique'),
(2, 'status'),
(3, 'special');

-- --------------------------------------------------------

--
-- Structure de la table `pokemon`
--

CREATE TABLE `pokemon` (
  `id_pokemon` int(3) NOT NULL,
  `numero_pokemon` int(3) UNSIGNED ZEROFILL NOT NULL,
  `nom_pokemon` varchar(255) NOT NULL,
  `type_pokemon` varchar(255) NOT NULL,
  `talent_pokemon` varchar(255) NOT NULL,
  `attaque_pokemon` text,
  `image_pokemon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pokemon`
--

INSERT INTO `pokemon` (`id_pokemon`, `numero_pokemon`, `nom_pokemon`, `type_pokemon`, `talent_pokemon`, `attaque_pokemon`, `image_pokemon`) VALUES
(5, 005, 'Reptincel', 'a:1:{i:0;s:1:\"7\";}', 'a:2:{i:0;s:1:\"4\";i:1;s:1:\"5\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"3\";}', ''),
(6, 001, 'Bulbizarre', 'a:2:{i:0;s:2:\"11\";i:1;s:2:\"12\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', ''),
(7, 002, 'Herbizarre', 'a:2:{i:0;s:2:\"11\";i:1;s:2:\"12\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', ''),
(8, 003, 'Florizarre', 'a:2:{i:0;s:2:\"11\";i:1;s:2:\"12\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', ''),
(9, 004, 'Salameche', 'a:1:{i:0;s:1:\"7\";}', 'a:2:{i:0;s:1:\"4\";i:1;s:1:\"5\";}', 'a:2:{i:0;s:1:\"2\";i:1;s:1:\"3\";}', ''),
(10, 006, 'Dracaufeu', 'a:2:{i:0;s:1:\"7\";i:1;s:2:\"18\";}', 'a:2:{i:0;s:1:\"4\";i:1;s:1:\"5\";}', 'a:2:{i:0;s:1:\"2\";i:1;s:1:\"3\";}', ''),
(11, 007, 'Carapuce', 'a:1:{i:0;s:1:\"4\";}', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"7\";}', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"5\";}', ''),
(12, 008, 'Carabaffe', 'a:1:{i:0;s:1:\"4\";}', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"7\";}', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"5\";}', ''),
(13, 009, 'Tortank', 'a:1:{i:0;s:1:\"4\";}', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"7\";}', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"5\";}', ''),
(14, 025, 'Pikachu', 'a:1:{i:0;s:1:\"5\";}', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'http://localhost/api-pokedex/upload/300px-Pikachu_SSB4.png');

-- --------------------------------------------------------

--
-- Structure de la table `talent`
--

CREATE TABLE `talent` (
  `id_talent` int(11) NOT NULL,
  `nom_talent` varchar(255) NOT NULL,
  `effetCmb_talent` text,
  `effetTrn_talent` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `talent`
--

INSERT INTO `talent` (`id_talent`, `nom_talent`, `effetCmb_talent`, `effetTrn_talent`) VALUES
(1, 'Engrais', 'Un Pokémon doté de ce talent verra la puissance de ses attaques de type Plante augmentée de 50% lorsque ses PV seront inférieurs à 1/3 de ses PV maximaux. Il existe trois autres talents à l\'effet similaire : Essaim, Torrent et Brasier.', NULL),
(2, 'Chlorophylle', 'Si le climat est ensoleillé, le Pokémon doté de Chlorophylle voit sa statistique de vitesse multipliée par deux.', NULL),
(4, 'Brasier', 'Un Pokémon doté de ce talent verra la puissance de ses attaques de type Feu augmentée de 50 % lorsque ses PV seront inférieurs à 1/3 de ses PV maximaux. Il existe trois autres talents à l\'effet similaire : Essaim, Engrais et Torrent.', NULL),
(5, 'Force Soleil', 'Un Pokémon doté de ce talent verra sa statistique d\'attaque spéciale augmenter de 50% lorsque le climat est ensoleillé. En parallèle, il perdra 1/8è de ses PV maximums à la fin de chaque tour.', NULL),
(6, 'Torrent', 'Un Pokémon doté de ce talent verra la puissance de ses attaques de type Eau augmentée de 50% lorsque ses PV seront inférieurs à 1/3 de ses PV maximaux. Il existe trois autres talents à l\'effet similaire : Essaim, Engrais et Brasier.', NULL),
(7, 'Cuvette', 'Un Pokémon doté de ce talent récupère 1/16 de ses PV maximums à la fin de chaque tour, si le climat est pluvieux.\r\n\r\nIl est notable qu\'un talent avec les mêmes propriétés existe avec la grêle : Corps Gel.', NULL),
(8, 'Statik', 'Si un Pokémon utilise une attaque directe (et que celle-ci inflige bien des dégâts) sur un Pokémon doté de ce talent, il a 30 % de chances d\'être paralysé.\r\n\r\nContrairement à une idée reçue, Statik atteint les Pokémon de type Sol malgré le fait que la capacité soit généralement détenue par des Pokémon Électrik.', 'Si un Pokémon avec ce talent est placé en tête d\'équipe, vous aurez 50% de chances de plus de rencontrer un Pokémon de type Électrik.'),
(9, 'Paratonnerre', 'Jusqu\'à la cinquième génération\r\nSi un Pokémon adverse utilise une capacité de type Électrik lors d\'un combat en 2 vs 2, celle-ci se dirige forcément vers le Pokémon doté de Paratonnerre.\r\n\r\nCependant :\r\n\r\nceci n\'est vrai que pour les capacités dirigées contre une seule cible. Si l\'attaque doit toucher les deux Pokémon, elle touche les deux Pokémon ;\r\nen parallèle, que ce soit en combat simple ou double, toutes les capacités Électrique touchent la cible, peu importe la précision qui lui est attribuée.\r\n\r\nDepuis la cinquième génération\r\n\r\nÀ partir de Pokémon Noir et Blanc, Paratonnerre immunise aux capacités Électrique (aussi bien en terme de dégâts infligés et d\'effets), et, en complément, augmente d\'un niveau l\'attaque spéciale si son porteur en reçoit une.\r\n\r\nCet effet la rend ainsi assez proche du talent Motorisé.\r\n\r\nCependant, l\'immunité du type Sol aux attaques de type Électrik prime sur la hausse d\'attaque spéciale provoquée par ce talent. En combat, utiliser Tonnerre ou Cage-Éclair sur un Pokémon Sol n\'activera donc pas ce talent. Néanmoins, utiliser Cage-Éclair sur un Pokémon Électrik doté de Paratonnerre activera quand même ce talent.\r\n\r\nDepuis la septième génération\r\n\r\nÀ partir de Pokémon Soleil et Lune, l\'immunité au type Sol ne prime plus sur le talent, ainsi, utiliser une attaque de type Électrik sur un Pokémon de type Sol possédant ce talent augmentera sa statistique d\'attaque spéciale. Cet effet fonctionne également si l\'autre Pokémon en combat double est aussi de type Sol.', 'Dans Pokémon Émeraude uniquement, Paratonnerre augmente les chances de recevoir des appels téléphoniques sur le PokéNav.');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `id_type` int(3) NOT NULL,
  `nom_type` varchar(255) NOT NULL,
  `image_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`id_type`, `nom_type`, `image_type`) VALUES
(1, 'Acier', 'http://localhost/api-pokedex/include/spriteType/Acier.png'),
(2, 'Combat', 'http://localhost/api-pokedex/include/spriteType/Combat.png'),
(3, 'Dragon', 'http://localhost/api-pokedex/include/spriteType/Dragon.png'),
(4, 'Eau', 'http://localhost/api-pokedex/include/spriteType/Eau.png'),
(5, 'Electrik', 'http://localhost/api-pokedex/include/spriteType/Electrik.png'),
(6, 'Fée', 'http://localhost/api-pokedex/include/spriteType/Fee.png'),
(7, 'Feu', 'http://localhost/api-pokedex/include/spriteType/Feu.png'),
(8, 'Glace', 'http://localhost/api-pokedex/include/spriteType/Glace.png'),
(9, 'Insecte', 'http://localhost/api-pokedex/include/spriteType/Insecte.png'),
(10, 'Normal', 'http://localhost/api-pokedex/include/spriteType/Normal.png'),
(11, 'Plante', 'http://localhost/api-pokedex/include/spriteType/Plante.png'),
(12, 'Poison', 'http://localhost/api-pokedex/include/spriteType/Poison.png'),
(13, 'Psy', 'http://localhost/api-pokedex/include/spriteType/Psy.png'),
(14, 'Roche', 'http://localhost/api-pokedex/include/spriteType/Roche.png'),
(15, 'Sol', 'http://localhost/api-pokedex/include/spriteType/Sol.png'),
(16, 'Spectre', 'http://localhost/api-pokedex/include/spriteType/Spectre.png'),
(17, 'Ténèbres', 'http://localhost/api-pokedex/include/spriteType/Tenebres.png'),
(18, 'Vol', 'http://localhost/api-pokedex/include/spriteType/Vol.png');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `capacite`
--
ALTER TABLE `capacite`
  ADD PRIMARY KEY (`id_capacite`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id_pokemon`);

--
-- Index pour la table `talent`
--
ALTER TABLE `talent`
  ADD PRIMARY KEY (`id_talent`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id_type`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `capacite`
--
ALTER TABLE `capacite`
  MODIFY `id_capacite` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `id_pokemon` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `talent`
--
ALTER TABLE `talent`
  MODIFY `id_talent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `id_type` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
