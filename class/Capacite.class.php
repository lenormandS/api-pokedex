<?php
/**
 *
 */
class capacite
{
  private $_db;
  function __construct($database)
  {
    $this->_db = $database;
  }

  public function getDb(){
    return $this->_db;
  }

  public function getAllCapacite(){
    $req = $this->getDb()->query("SELECT * FROM capacite");
    $req->execute();
    return $req->fetchAll();
  }

  public function addCategorieDb($array){
    $nom = $array['nom_capacite'];
    $type = $array['type'];
    $categorie = $array['categorie'];
    $puissance = intval($array['puissance']);
    $precision = intval($array['precision']);
    $pp = intval($array['pp']);

    $sql = "INSERT INTO capacite (nom,type,categorie,puissance,precision_cap,pp) VALUES (
      '$nom',
      $type,
      $categorie,
      $puissance,
      $precision,
      $pp
    )";
    $this->getDb()->query($sql);
  }
  public function getCapaciteById($val){
    $req = "SELECT nom,type,categorie,puissance,precision_cap,pp FROM capacite WHERE id_capacite = $val";
    $res = $this->getDb()->prepare($req);
    $res->execute();
    return $res->fetchAll();
  }
}
