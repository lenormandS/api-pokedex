<?php
/**
 *
 */
class pokemon
{
  private $_db;
  private $_path;
  function __construct($database,$path)
  {
    $this->_db = $database;
    $this->_path = $path;
  }

  public function getPath(){
    return $this->_path;
  }
  public function getDb(){
    return $this->_db;
  }

  public function addPokemonDb($array){
    $file_tmp = $array['img']['tmp_name'];
    $filename = $array['img']['name'];
    $dir = '../upload/';
    $db_path = $this->getPath()."upload/".$filename;
    move_uploaded_file($file_tmp, $dir.$filename);
    $numero = intval($array['numero_pokemon']);
    $nom = $array['nom_pokemon'];
    $type = serialize($array['type']);
    $talent = serialize($array['talent']);
    $attaque = serialize($array['capacite']);
    $stat = serialize(array("PV" => $array['pv'],
                            "Attaque" => $array['attaque'],
                            "Defense" => $array['defense'],
                             "AtkSpe" => $array['atksp'],
                            "DefSpe" => $array['defsp'],
                            "Vitesse" => $array['vitesse']));
    // echo '<pre>';
    // print_r($stat);
    // echo '</pre>';
    // die();
    $sql = "INSERT INTO pokemon (numero_pokemon,nom_pokemon,type_pokemon,talent_pokemon,attaque_pokemon,stat,image_pokemon) VALUES ('$numero',
      '$nom',
      '$type',
      '$talent',
      '$attaque',
      '$stat',
      '$db_path')";
    $this->getDb()->query($sql);
  }

  public function getAllPokemon(){
    $sql = "SELECT id_pokemon, numero_pokemon, nom_pokemon,type_pokemon,image_pokemon FROM pokemon ORDER BY numero_pokemon ASC";
    $req = $this->getDb()->query($sql);
    $req->execute();
    return $pokemon = $req->fetchAll();
  }

  public function getPokemon($nombre){
    $sql = "SELECT * FROM pokemon WHERE id_pokemon = $nombre";
    $req = $this->getDb()->query($sql);
    $req->execute();
    return $pokemon = $req->fetchAll();
  }

  public function getListPokemon(){
    $sql = "SELECT id_pokemon,numero_pokemon, nom_pokemon FROM pokemon ORDER BY numero_pokemon ASC";
    $req = $this->getDb()->query($sql);
    $req->execute();
    return $req->fetchAll();
  }
  public function delPokemon($int){
    try {
      $req = "DELETE FROM pokemon WHERE id_pokemon = $int";
      $this->getDb()->query($req);
      return "pokemon enlevé de la base";
    } catch (Exception $e) {
      return "erreur : ". $e->getMessage();
    }
  }
}
