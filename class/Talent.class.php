<?php
/**
 *
 */
class talent
{
  private $_db;
  function __construct($database)
  {
    $this->_db = $database;
  }

  public function getDb(){
    return $this->_db;
  }

  public function getAllTalent(){
    $req = $this->getDb()->query("SELECT * FROM talent");
    $req->execute();
    return $req->fetchAll();
  }

  public function addTalentDb($array){
    $nom = $array['talent'];
    if(empty($array['effetCmb'])){
        $effetCmb = "NULL";
    }else{
      $effetCmb = $array['effetCmb'];
    }
    if(empty($array['effetTrn'])){
        $effettrn = "NULL";
    }else{
      $effettrn = $array['effetTrn'];
    }
    $sql = "INSERT INTO talent (nom_talent,effetCmb_talent,effetTrn_talent) VALUES ('$nom',
      $effetCmb,
      $effettrn)";
    $this->getDb()->query($sql);

  }
  
  public function getTalentById($val){
    $req = "SELECT nom_talent FROM talent WHERE id_talent = $val";
    $res = $this->getDb()->prepare($req);
    $res->execute();
    return $res->fetchAll();
  }

}
