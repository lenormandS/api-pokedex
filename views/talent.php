<?php
require_once '../include/header.php';
if(isset($_POST) && !empty($_POST)){
  $Talent->addTalentDb($_POST);
}
?>
<form class="" method="post">
  <div class="form-group">
    <label>Nom talent</label>
    <input type="text" placeholder="Nom talent" name="talent" class="form-control">
  </div>
  <div class="form-group">
<label>Effet Combat</label>
<textarea name="effetCmb" rows="8" cols="80" class="form-control"></textarea>
  </div>
  <div class="form-group">
<label>Effet Terrain</label>
<textarea name="effetTrn" rows="8" cols="80" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <input type="submit" value="Valider" class="btn btn-success">
  </div>
</form>
<?php
require_once '../include/footer.php';
?>
