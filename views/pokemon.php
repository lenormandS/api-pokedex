<?php
require_once '../include/header.php';
  if(isset($_GET['del']) && !empty($_GET['del'])){
    $pokemon->delPokemon($_GET['del']);
    die();
  }
  $list = $pokemon->getAllPokemon();
 ?>
 <div class="table">
   <table class="table table-stripped">
     <tr>
       <th>Numero Pokemon</th>
       <th>Nom Pokemon</th>
       <th>Type Pokemon</th>
       <th>Action</th>
     </tr>
     <tr>
       <?php foreach ($list as $key => $value) {
         ?>
         <td><?php echo $value['numero_pokemon']?></td>
         <td><?php echo $value['nom_pokemon']?></td>
         <td>
           <ul class="list-group">
             <?php
             $un = unserialize($value['type_pokemon']);
             foreach ($un as $key => $value) {
               $test = $Type->getTypeById($value);
               foreach ($test as $ke => $val) {
                 ?>
                 <li class="list-group-item"><img src="<?php echo $val['image_type']?>" alt="<?php echo $val['nom_type']?>"></li>
                 <?php
               }
             }
             ?>
           </ul>
         </td>
         <td>
           <button type="button" id="<?php echo $value['id_pokemon']?>" class="btn btn-danger btn-del"><span class="glyphicon glyphicon-trash"></span></button>
         </td>
       </tr>
       <?php
     }
     ?>
   </table>
 </div>
 <script>
 $(".btn-del").on('click',function(){
   var id = $(this).attr('id');
   if(confirm('supprimer le pokemon?')){
     $.ajax({
       method:'GET',
       data: 'del='+id,
       success:function(res){
         alert('Le pokemon a bien été enlevé de la base');
         location.reload();
       }
     })
   }else{
     return false;
   }
 })
</script>
<?php
  require_once '../include/footer.php';
 ?>
