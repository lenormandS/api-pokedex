<?php
require_once '../include/header.php';


$api = $pokemon->getPokemon("14");
foreach ($api as $key => $value) {
  $obj['numero'] = $value['numero_pokemon'];
  $obj['nom'] = $value['nom_pokemon'];
  $obj['image'] = $value['image_pokemon'];
  $obj['type'] = [];
  $obj['image_type'] = [];
  $obj['talent'] = [];
  $obj['capacite'] = [];
  foreach (unserialize($value['type_pokemon']) as $key => $type_id) {
    foreach ($Type->getTypeById($type_id) as $ke => $val) {
      array_push($obj['type'] ,$val['nom_type']);
      array_push($obj['image_type'] ,$val['image_type']);
    }
  }
  foreach (unserialize($value['talent_pokemon']) as $key => $talent_id) {
    foreach ($Talent->getTalentById($talent_id) as $key => $val) {
      array_push($obj['talent'],$val['nom_talent']);
    }
  }
  foreach (unserialize($value['attaque_pokemon']) as $key => $capacite_id) {
    foreach ($Capacite->getCapaciteById($capacite_id) as $key => $val) {
      array_push($obj['capacite'],$val);
    }
  }
}
echo '<pre>';
print_r($obj);
echo '</pre>';


require_once '../include/footer.php';
 ?>
