<?php
require_once '../include/header.php';

if(isset($_POST) && !empty($_POST)){
  $_POST['img'] = $_FILES['img'];
  $pokemon->addPokemonDb($_POST);
}
?>
        <form method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label>Image pokemon</label>
            <input type="file" name="img" class="form-control">
          </div>
          <div class="form-group">
            <label>Numero Pokemon</label>
            <input type="text" name="numero_pokemon" placeholder="Numero du pokemon" class="form-control">
          </div>
          <div class="form-group">
            <label>Nom Pokemon</label>
            <input type="text" name="nom_pokemon" placeholder="Nom du pokemon" class="form-control">
          </div>
          <div class="form-group">
            <label>Type</label>
            <select multiple class="form-control" name="type[]">
                <?php
                foreach ($type as $key) {
                  ?>
                  <option value="<?php echo $key['id_type']?>"><?php echo $key['nom_type']?></option>
                  <?php
                }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label>Talent</label>
            <select class="form-control" multiple name="talent[]">
              <?php
              foreach ($talent as $key) {
                ?>
                <option value="<?php echo $key['id_talent']?>"><?php echo $key['nom_talent']?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Attaque</label>
            <select class="form-control" multiple name="capacite[]">
              <?php
              foreach ($capacite as $key) {
                ?>
                <option value="<?php echo $key['id_capacite']?>"><?php echo $key['nom']?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <div class="form-inline">
              <label>PV</label>
              <input type="text" name="pv" class="form-control">
              <label>Attaque</label>
              <input type="text" name="attaque" class="form-control">
              <label>Defense</label>
              <input type="text" name="defense" class="form-control">
            </div>
            <div class="form-inline">
              <label>Attaque Spéciale</label>
              <input type="text" name="atksp" class="form-control">
              <label>Defense Spéciale</label>
              <input type="text" name="defsp" class="form-control">
              <label>Vitesse</label>
              <input type="text" name="vitesse" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <input type="submit" value="Valider" class="btn btn-success">
          </div>
        </form>
  <?php
require_once '../include/footer.php';
  ?>
