<?php

require_once '../include/header.php';
if(isset($_POST) && !empty($_POST)){
  $Capacite->addCategorieDb($_POST);
}

?>
  <form method="post">
    <div class="form-group">
      <label>Nom Capacité</label>
      <input type="text" name="nom_capacite" placeholder="Nom capacité" class="form-control">
    </div>
    <div class="form-group">
      <label>Type Capacité</label>
      <select class="form-control" name="type">
        <?php
        foreach ($type as $key) {
          ?>
          <option value="<?php echo $key['nom_type']?>"><?php echo $key['nom_type']?></option>
          <?php
        }
        ?>
      </select>
    </div>
    <div class="form-group">
      <label>Categorie Capacité</label>
      <select class="form-control" name="categorie">
        <?php
        foreach ($categorie as $value) {
          ?>
          <option value="<?php echo $value['nom_categorie']?>"><?php echo $value['nom_categorie']?></option>
          <?php
        }
        ?>
      </select>
    </div>
    <div class="form-group">
      <label>Puissance</label>
      <input type="text" name="puissance" placeholder="de 0 a 200" class="form-control">
    </div>
    <div class="form-group">
      <label>Precision</label>
      <input type="text" name="precision" placeholder="de 0 a 200" class="form-control">
    </div>
    <div class="form-group">
      <label>PP (point pouvoir)</label>
      <select class="form-control" name="pp">
        <option value="1">1</option>
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="15">15</option>
        <option value="20">20</option>
        <option value="25">25</option>
        <option value="30">30</option>
        <option value="35">35</option>
        <option value="40">40</option>
      </select>
    </div>
    <div class="form-group">
      <input type="submit" value="Valider" class="btn btn-success">
      <input type="reset" value="Annulez" class="btn btn-danger">
    </div>
  </form>
<?php require_once '../include/footer.php'?>
