<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

require_once '../config.php';

$postRequest = json_decode(file_get_contents("php://input"));

if(isset($postRequest->action) && $postRequest->action == "getAllPokemon"){
  $api = $pokemon->getAllPokemon();
  $list = [];
  foreach ($api as $key => $value) {
    $obj['id'] = $value['id_pokemon'];
    $obj['numero'] = $value['numero_pokemon'];
    $obj['nom'] = $value['nom_pokemon'];
    $obj['image'] = $value['image_pokemon'];
    $un = unserialize($value['type_pokemon']);
    $obj['type'] = [];
    $obj['image_type'] = [];
    foreach ($un as $key => $value) {
      $test = $Type->getTypeById($value);
      foreach ($test as $ke => $val) {
        array_push($obj['type'] ,$val['nom_type']);
        array_push($obj['image_type'] ,$val['image_type']);
      }
    }
    array_push($list, $obj);
  }
  echo json_encode($list);
}
if(isset($postRequest->action) && $postRequest->action == "getPokemon"){
  $api = $pokemon->getPokemon($postRequest->nombre);
  foreach ($api as $key => $value) {
    $obj['numero'] = $value['numero_pokemon'];
    $obj['nom'] = $value['nom_pokemon'];
    $obj['image'] = $value['image_pokemon'];
    $obj['type'] = [];
    $obj['talent'] = [];
    $obj['capacite'] = [];
    foreach (unserialize($value['type_pokemon']) as $key => $type_id) {
      foreach ($Type->getTypeById($type_id) as $ke => $val) {
        array_push($obj['type'] ,$val['image_type']);
      }
    }
    foreach (unserialize($value['talent_pokemon']) as $key => $talent_id) {
      foreach ($Talent->getTalentById($talent_id) as $key => $val) {
        array_push($obj['talent'],$val['nom_talent']);
      }
    }
    foreach (unserialize($value['attaque_pokemon']) as $key => $capacite_id) {
      foreach ($Capacite->getCapaciteById($capacite_id) as $key => $val) {
        array_push($obj['capacite'],$val);
      }
    }
    foreach (unserialize($value['stat']) as $key => $stat) {
      $obj[$key] = [];
      array_push($obj[$key],$stat);
    }
  }
  echo json_encode($obj);
}
if(isset($postRequest->action) && $postRequest->action == "getType"){
  echo json_encode($type);
}
if(isset($postRequest->action) && $postRequest->action == "getAllTalent"){
  echo json_encode($talent);
}
